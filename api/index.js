import express from 'express'
import templates from './db/templates.json'

const app = express()
app.use(express.json())

app.get('/data', (req, res) => {
  const { offset, count } = req.query
  const data = templates.slice(
    parseInt(offset),
    parseInt(offset) + parseInt(count)
  )
  res.send({ items: data })
})

app.get('/count', (_, res) => {
  res.send({ count: templates.length })
})

export default app
